const db = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const refreshTokenService = require("../services/refreshToken.service");

const signUp = async (req, res) => {
    try {
        const userRole = await db.role.findOne({
            name: "user"
        })
    
        const user = new db.user({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 8),
            roles: [
                userRole._id
            ]
        })
    
        await user.save();

        res.status(200).json({
            message: "Create user successfully"
        })
    } catch (error) {
        res.status(500).json({
            message: "Intenal server error"
        })
    }
}

const logIn = async (req, res) => {
    try {
        const existUser = await db.user.findOne({
            username: req.body.username
        }).populate("roles");
        console.log(existUser);
        if(!existUser) {
            return res.status(404).send({
                message: "User not found"
            })
        }
        
        var passwordIsValid = bcrypt.compareSync(
            req.body.password,
            existUser.password
        )

        if(!passwordIsValid) {
            return res.status(401).json({
                message: "Invalid password"
            })
        }

        const secretKey = process.env.JWT_SECRET;
        
        const token = jwt.sign({id: existUser._id}, secretKey, {
            algorithm: "HS256",
            allowInsecureKeySizes: true,
            expiresIn: process.env.JWT_EXPIRATION // 1 ngày 
        });

        // Sinh thêm refresh token
        const refreshToken = await refreshTokenService.createToken(existUser);

        res.status(200).json({
            accessToken: token,
            refreshToken: refreshToken
        })
    } catch (error) {
        console.error(error);

        res.status(500).json({
            message: "Intenal server error"
        })
    }
}

const refreshToken = async (req, res) => {
    const { refreshToken } = req.body;

    if(refreshToken == null) {
        return res.status(403).json({
            message: "Refresh token is required!"
        })
    }

    try {
        const refreshTokenObj = await db.refreshToken.findOne({
            token: refreshToken
        });   

        if(!refreshTokenObj) {
            return res.status(403).json({
                message: "Refresh token not found!"
            })
        }

        if(refreshTokenObj.expiredDate.getTime() < new Date().getTime()) {
            // Refresh token đã hết hạn
            await db.refreshToken.findByIdAndRemove(refreshTokenObj._id);

            return res.status(403).json({
                message: "Refresh token was expired!"
            })
        }

        const secretKey = process.env.JWT_SECRET;

        const newAccessToken = jwt.sign({id: refreshTokenObj.user}, secretKey, {
            algorithm: "HS256",
            allowInsecureKeySizes: true,
            expiresIn: process.env.JWT_EXPIRATION // 1 ngày 
        });

        return res.status(200).json({
            accessToken: newAccessToken,
            refreshToken: refreshTokenObj.token
        })
        
    } catch (error) {
        console.error(error);

        res.status(500).json({
            message: "Intenal server error"
        })
    }
}

module.exports = {
    signUp,
    logIn,
    refreshToken
}